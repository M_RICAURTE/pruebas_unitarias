/*
 * PruebaUnitaria 2017 - All rights reserved
 * http://mardz.com
 * Develop by: Marcos Ricaurte de Zubiria
 * File encode: UTF-8
 */
package com.mardz.prueba_unitaria;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 * @author <a href="mailto:marcos.a.ricaurte@gmail.com">Marcos Ricaurte</a>
 */
public class CalculadoraAvanzadaTest {

    public CalculadoraAvanzadaTest() {
    }

    static CalculadoraAvanzada calc;

    @BeforeClass
    public static void beforeClass() {
        System.out.println("Prueba del metodo ---> BeforeClass()");
        calc = new CalculadoraAvanzada();
    }

    @Before
    public void before() {
        System.out.println("Prueba del metodo Before()");
        calc = new CalculadoraAvanzada();
    }

    @After
    public void after() {
        System.out.println("Prueba del metodo After()");
    }

    @AfterClass
    public static void afterClass() {
        System.out.println("Prueba del metodo ---> AfterClass()");
        calc.clear();
    }

    @Test
    public void testSum() {
        System.out.println("Prueba del metodo ---> testSum()");
        int resultado = calc.add(3, 2);
        int esperado = 5;
        assertEquals(esperado, resultado);
    }

    @Test
    public void testAnsSum() {
        System.out.println("Prueba del metodo ---> testAnsSum()");
        int resultado = calc.add(3, 2);
        int esperado = 5;
        assertEquals(esperado, resultado);
    }

    @Test
    public void testDiv() {
        System.out.println("Prueba del metodo ---> testDiv()");
        calc.div(5, 2);
    }

    @Test(expected = ArithmeticException.class)
    public void testDivPorCero() {
        System.out.println("Prueba del metodo ---> testDivPorCero()");
        calc.div(5, 0);
    }

    @Test(timeout = 2500)
    public void testOperacionOptima() {
        System.out.println("Prueba del metodo ---> testOperacionOptima()");
        calc.operacionOptima();
    }
}