/*
 * PruebaUnitaria 2017 - All rights reserved
 * http://mardz.com
 * Develop by: Marcos Ricaurte de Zubiria
 * File encode: UTF-8
 */
package com.mardz.prueba_unitaria;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author <a href="mailto:marcos.a.ricaurte@gmail.com">Marcos Ricaurte</a>
 */
public class AssertTest {

    public AssertTest() {
    }

    /**
     * Simple comparison test
     */
    @Test
    public void test() {
        assertTrue(true);
        assertNotEquals("3 NO es igual a 3", 3, 4);
        String s1 = "Hola";
        String s2 = "Hola";
        assertEquals(s1, s2);
        assertSame(s1, s2);
    }
}