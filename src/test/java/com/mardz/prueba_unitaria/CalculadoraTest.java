/*
 * PruebaUnitaria 2017 - All rights reserved
 * http://mardz.com
 * Develop by: Marcos Ricaurte de Zubiria
 * File encode: UTF-8
 */
package com.mardz.prueba_unitaria;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Test Calculation program
 *
 * @author <a href="mailto:marcos.a.ricaurte@gmail.com">Marcos Ricaurte</a>
 */
public class CalculadoraTest {

    public CalculadoraTest() {
    }

    @Test
    public void testSumar() {
        int resultado = Calculadora.sumar(2, 3);
        int esperado = 5;
        assertEquals(esperado, resultado);
    }

    @Test
    public void testRestar() {
        int resultado = Calculadora.restar(3, 2);
        int esperado = 1;
        assertEquals(esperado, resultado);
    }
}