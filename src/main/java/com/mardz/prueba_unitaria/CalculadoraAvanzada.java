/*
 * PruebaUnitaria 2017 - All rights reserved
 * http://mardz.com
 * Develop by: Marcos Ricaurte de Zubiria
 * File encode: UTF-8
 */
package com.mardz.prueba_unitaria;

/**
 * Calculation program
 *
 * @author <a href="mailto:marcos.a.ricaurte@gmail.com">Marcos Ricaurte</a>
 */
public class CalculadoraAvanzada {

    /**
     * Reference to the previous answer
     */
    private int ans;

    public CalculadoraAvanzada() {
        ans = 0;
    }

    /**
     * Procedure to add two numbers
     *
     * @param a First adding
     * @param b Second adding
     * @return Sum total
     */
    public int add(int a, int b) {
        ans = a + b;
        return ans;
    }

    /**
     * Subtraction
     *
     * @param a Number to which in the arithmetic operation of the subtraction
     * is removed another to obtain the result or difference.
     * @param b Subtracting
     * @return Total of the subtraction
     */
    public int sub(int a, int b) {
        ans = a - b;
        return ans;
    }

    /**
     * Procedure to add a number to the accumulated
     *
     * @param v Number to subtract to the accumulated
     * @return Sum total
     */
    public int add(int v) {
        ans += v;
        return ans;
    }

    /**
     * Procedure to subtract a number from the accumulated
     *
     * @param v Number to add to the accumulated
     * @return Total of the subtraction
     */
    public int sub(int v) {
        ans -= v;
        return ans;
    }

    /**
     * division
     *
     * @param a dividend
     * @param b divider can not be zero
     * @return quotient
     */
    public int div(int a, int b) {
        if (b == 0) {
            throw new ArithmeticException("No puedes dividir por cero!!!");
        }
        ans = a / b;
        return ans;
    }

    /**
     * previous answer
     *
     * @return previous answer
     */
    public int ans() {
        return ans;
    }

    /**
     * Clean the accumulator
     */
    public void clear() {
        ans = 0;
    }

    /**
     * Test for a 2-second delay
     */
    public void operacionOptima() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
        }
    }
}