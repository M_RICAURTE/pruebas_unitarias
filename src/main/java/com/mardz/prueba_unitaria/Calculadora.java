/*
 * PruebaUnitaria 2017 - All rights reserved
 * http://mardz.com
 * Develop by: Marcos Ricaurte de Zubiria
 * File encode: UTF-8
 */
package com.mardz.prueba_unitaria;

/**
 * Calculation program
 *
 * @author <a href="mailto:marcos.a.ricaurte@gmail.com">Marcos Ricaurte</a>
 */
public class Calculadora {

    /**
     * Add two numbers.
     *
     * @param a First integer
     * @param b Second integer
     * @return Total of the sum
     */
    public static int sumar(int a, int b) {
        return a + b;
    }

    /**
     * Subtraction
     *
     * @param minuendo Number to which in the arithmetic operation of the
     * subtraction is removed another to obtain the result or difference.
     * @param sustraendo Subtracting
     * @return Total of the subtraction
     */
    public static int restar(int minuendo, int sustraendo) {
        return minuendo - sustraendo;
    }

    /**
     * Perform a manual test
     *
     * @param args Has no external parameters
     */
    public static void main(String[] args) {
        int a = 3, b = 2;
        int res = sumar(a, b);
        int esperado = 5;
        if (res == esperado) {
            System.out.println("El programa esta bien hecho");
        } else {
            System.out.println("El programa esta MAL hecho");
        }
    }
}
